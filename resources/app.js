import PhpEngine from "./PhpEngine.js";

const php = new PhpEngine

php.addEventListener("ready", () => {
    php.download("./phpdata/latte.data.js", () => compile())
})

function compile() {
    let version = "2.10.3";
    let root = document.querySelector("[data-latte-root]").dataset["latteRoot"];
    let templates = JSON.parse(document.querySelector("[data-latte-templates]").dataset["latteTemplates"]);

    Promise.all(templates.map(async (template) => {
        return new Promise(resolve => {
            fetch(root + template).then(response => response.text()).then(body => {
                resolve({[template]: body})
            })
        })
    })).then(templates => {
        let data = {};

        templates.map((template) => data[Object.keys(template)[0]] = template[Object.keys(template)[0]])

        data = JSON.stringify(data);

        let r = `require 'phar://latte-v${version}.phar/vendor/autoload.php'; require 'phar://app.phar/src/index.php'; playground('${data}', '${JSON.stringify({"sandbox":false})}');`;

        php.run(r).then(t => {
            if (typeof JSON.parse(php.stdout).warnings.length === "undefined" || typeof JSON.parse(php.stdout).errors.length === "undefined") {
                document.body.innerHTML = JSON.stringify(JSON.parse(php.stdout).warnings.output) + "<br>" + JSON.stringify(JSON.parse(php.stdout).errors.output)
            } else {
                document.documentElement.innerHTML = JSON.parse(php.stdout).output;
            }
        }).catch(t => {
            console.log(php.stderr)
        })
    })


}
