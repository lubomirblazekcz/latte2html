export default function (a) {
    a.expectedDataFileDownloads++;
    !function (e) {
        if ("object" == typeof window) window.encodeURIComponent(window.location.pathname.toString().substring(0, window.location.pathname.toString().lastIndexOf("/")) + "/"); else {
            if ("undefined" == typeof location) throw"using preloaded data can only be done on a web page or in a web worker";
            encodeURIComponent(location.pathname.toString().substring(0, location.pathname.toString().lastIndexOf("/")) + "/")
        }
        var t = "dist/latte.data";
        "function" != typeof a.locateFilePackage || a.locateFile || (a.locateFile = a.locateFilePackage, err("warning: you defined Module.locateFilePackage, that has been renamed to Module.locateFile (using your locateFilePackage for now)"));
        var n = a.locateFile ? a.locateFile("latte.data", "") : "latte.data", o = e.remote_package_size;
        e.package_uuid;
        var l, d, i, r, s = null, u = a.getPreloadedPackage ? a.getPreloadedPackage(n, o) : null;

        function f() {
            function n(a, e) {
                if (!a) throw e + (new Error).stack
            }

            function o(a, e, t) {
                this.start = a, this.end = e, this.audio = t
            }

            o.prototype = {
                requests: {}, open: function (e, t) {
                    this.name = t, this.requests[t] = this, a.addRunDependency("fp " + this.name)
                }, send: function () {
                }, onload: function () {
                    var a = this.byteArray.subarray(this.start, this.end);
                    this.finish(a)
                }, finish: function (e) {
                    a.FS_createDataFile(this.name, null, e, !0, !0, !0), a.removeRunDependency("fp " + this.name), this.requests[this.name] = null
                }
            };
            for (var l = e.files, d = 0; d < l.length; ++d) new o(l[d].start, l[d].end, l[d].audio).open("GET", l[d].filename);

            function i(t) {
                n(t, "Loading data file failed."), n(t instanceof ArrayBuffer, "bad input to processPackageData");
                var l = new Uint8Array(t);
                o.prototype.byteArray = l;
                for (var d = e.files, i = 0; i < d.length; ++i) o.prototype.requests[d[i].filename].onload();
                a.removeRunDependency("datafile_dist/latte.data")
            }

            a.addRunDependency("datafile_dist/latte.data"), a.preloadResults || (a.preloadResults = {}), a.preloadResults[t] = {fromCache: !1}, u ? (i(u), u = null) : s = i
        }

        u || (l = n, d = o, i = function (a) {
            s ? (s(a), s = null) : u = a
        }, (r = new XMLHttpRequest).open("GET", l, !0), r.responseType = "arraybuffer", r.onprogress = function (e) {
            var t = l, n = d;
            if (e.total && (n = e.total), e.loaded) {
                r.addedTotal ? a.dataFileDownloads[t].loaded = e.loaded : (r.addedTotal = !0, a.dataFileDownloads || (a.dataFileDownloads = {}), a.dataFileDownloads[t] = {
                    loaded: e.loaded,
                    total: n
                });
                var o = 0, i = 0, s = 0;
                for (var u in a.dataFileDownloads) {
                    var f = a.dataFileDownloads[u];
                    o += f.total, i += f.loaded, s++
                }
                o = Math.ceil(o * a.expectedDataFileDownloads / s), a.setStatus && a.setStatus("Downloading data... (" + i + "/" + o + ")")
            } else a.dataFileDownloads || a.setStatus && a.setStatus("Downloading data...")
        }, r.onerror = function (a) {
            throw new Error("NetworkError for: " + l)
        }, r.onload = function (a) {
            if (!(200 == r.status || 304 == r.status || 206 == r.status || 0 == r.status && r.response)) throw new Error(r.statusText + " : " + r.responseURL);
            var e = r.response;
            i(e)
        }, r.send(null)), a.calledRun ? f() : (a.preRun || (a.preRun = []), a.preRun.push(f))
    }({
        files: [{filename: "/latte-v2.4.9.phar", start: 0, end: 155170, audio: 0}, {
            filename: "/latte-v2.5.0.phar",
            start: 155170,
            end: 308405,
            audio: 0
        }, {filename: "/latte-v2.5.1.phar", start: 308405, end: 461408, audio: 0}, {
            filename: "/latte-v2.5.2.phar",
            start: 461408,
            end: 614351,
            audio: 0
        }, {filename: "/latte-v2.5.3.phar", start: 614351, end: 767528, audio: 0}, {
            filename: "/latte-v2.5.4.phar",
            start: 767528,
            end: 920826,
            audio: 0
        }, {filename: "/latte-v2.5.5.phar", start: 920826, end: 1074185, audio: 0}, {
            filename: "/latte-v2.5.6.phar",
            start: 1074185,
            end: 1227660,
            audio: 0
        }, {filename: "/latte-v2.6.0.phar", start: 1227660, end: 1382524, audio: 0}, {
            filename: "/latte-v2.6.1.phar",
            start: 1382524,
            end: 1537614,
            audio: 0
        }, {filename: "/latte-v2.6.2.phar", start: 1537614, end: 1692830, audio: 0}, {
            filename: "/latte-v2.6.3.phar",
            start: 1692830,
            end: 1848166,
            audio: 0
        }, {filename: "/latte-v2.7.0.phar", start: 1848166, end: 2011025, audio: 0}, {
            filename: "/latte-v2.7.1.phar",
            start: 2011025,
            end: 2174645,
            audio: 0
        }, {filename: "/latte-v2.7.2.phar", start: 2174645, end: 2338287, audio: 0}, {
            filename: "/latte-v2.7.3.phar",
            start: 2338287,
            end: 2502053,
            audio: 0
        }, {filename: "/latte-v2.8.0.phar", start: 2502053, end: 2677580, audio: 0}, {
            filename: "/latte-v2.8.1.phar",
            start: 2677580,
            end: 2854012,
            audio: 0
        }, {filename: "/latte-v2.8.2.phar", start: 2854012, end: 3030821, audio: 0}, {
            filename: "/latte-v2.8.3.phar",
            start: 3030821,
            end: 3207280,
            audio: 0
        }, {filename: "/latte-v2.8.4.phar", start: 3207280, end: 3384245, audio: 0}, {
            filename: "/latte-v2.8.5.phar",
            start: 3384245,
            end: 3561274,
            audio: 0
        }, {filename: "/latte-v2.9.0.phar", start: 3561274, end: 3752507, audio: 0}, {
            filename: "/latte-v2.9.1.phar",
            start: 3752507,
            end: 3946079,
            audio: 0
        }, {filename: "/latte-v2.9.2.phar", start: 3946079, end: 4141608, audio: 0}, {
            filename: "/latte-v2.9.3.phar",
            start: 4141608,
            end: 4337271,
            audio: 0
        }, {filename: "/latte-v2.10.0.phar", start: 4337271, end: 4541784, audio: 0}, {
            filename: "/latte-v2.10.1.phar",
            start: 4541784,
            end: 4746377,
            audio: 0
        }, {filename: "/latte-v2.10.2.phar", start: 4746377, end: 4955245, audio: 0}, {
            filename: "/latte-v2.10.3.phar",
            start: 4955245,
            end: 5165186,
            audio: 0
        }, {filename: "/latte-v2.10.x-dev.phar", start: 5165186, end: 5375167, audio: 0}, {
            filename: "/app.phar",
            start: 5375167,
            end: 5411403,
            audio: 0
        }], remote_package_size: 5411403, package_uuid: "a402b9da-337e-44f8-a4fd-c9dfb3cb28cb"
    })
};