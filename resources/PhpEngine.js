import PHP from "./php8/php.js";

export default class PhpEngine extends EventTarget {
    constructor() {
        super(), this.stdout = "", this.stderr = "", this.afterDownload = null;
        let t = {
            locateFile: (t, n) => (t.endsWith(".data") && (n = "/resources/phpdata/"), n + t),
            monitorRunDependencies: t => {
                0 === t && this.afterDownload && (this.afterDownload(), this.afterDownload = null)
            },
            postRun: () => {
                const t = new CustomEvent("ready");
                this.dispatchEvent(t)
            },
            print: (...t) => {
                this.stdout += t.map(t => t + "\n").join("")
            },
            printErr: (...t) => {
                this.stderr += t.map(t => t + "\n").join("")
            }
        };
        this.binary = new PHP(t).then(t => t, t => console.error(t))
    }

    download(t, n) {
        this.afterDownload = n, import(t).then(t => {
            t.default(this.binary)
        })
    }

    run(t) {
        return new Promise(n => {
            this.stdout = this.stderr = "", n(this.binary.ccall("pib_run", "number", ["string"], [t]))
        })
    }
};