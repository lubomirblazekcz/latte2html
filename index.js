import { startDevServer } from '@web/dev-server';
import puppeteer from 'puppeteer';

(async () => {
    const server = await startDevServer({
        config: {
            port: 8000
        },
        logStartMessage: false
    });
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(`http://localhost:8000/index.html`);

    await page.waitForFunction('document.body.innerHTML.length > 1');

    let data;

    const getData = async() => {
        return await page.evaluate(async () => {
            return await new Promise(resolve => {
                resolve("<!DOCTYPE html>"+document.documentElement.outerHTML)
            })
        })
    }

    data = await getData();

    console.log(data)

    await browser.close();
    await server.stop();
})();